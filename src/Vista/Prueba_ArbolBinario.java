/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Negocio.Arbol_Letras;

/**
 *
 * @author madar
 */
public class Prueba_ArbolBinario {

    public static void main(String[] args) {
        Arbol_Letras myArbol = new Arbol_Letras();
        myArbol.imprimir();
        System.out.println("Tiene :"+myArbol.getConHojas()+" hojas");
        System.out.println("Tiene :"+myArbol.getContarHojasVocales()+" hojas que contienen vocales");
        System.out.println("Tiene :"+myArbol.getCantidadHojas_Derechas()+" hojas derechas");
        System.out.println("Mi código Luka:"+myArbol.miCodigoLukasiewicz());
        System.out.println("Preorden:"+myArbol.getPre());
        System.out.println("Inorden:"+myArbol.getIn());
        System.out.println("Posorden:"+myArbol.getPos());
        System.out.println("Tiene : "+myArbol.getCardinalidad()+" nodos internos");
        System.out.println("Niveles:"+myArbol.getElementos_Nivel());

    }

}
